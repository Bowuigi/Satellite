"## ℹ About

Satellite is a social network based on Gemini, which is used for display, interaction and authentication.

This implementation is written in Chicken Scheme using Gemini CGI and the LMDB database (via the lmdb-ht egg).

Self hosting is encouraged.

Satellite was developed by Bowuigi, see the official repository and instance list on:

=> https://codeberg.org/Bowuigi/Satellite [HTTPS/Codeberg] Satellite repository

Contact: bowuigi (at) vern (dot) cc
"

