;; Where the Satellite database will be located.
;; The folder it is in must exist and you must have read/write permission.
;; Keep in mind that the database is a folder.
(define *database-location* "/etc/satellite-db")
