(import utf8
        common
        user
        (prefix db db/)
        (prefix gemini gemini/))

(db/init! *database-location*)
(ensure-on-exit! db/end!)

(gemini/init!)

(define url-path
  (if (not (gemini/empty? (gemini/*path*)))
    (gemini/parse-url-path (gemini/*path*))
    '()))

(define (list-users _)
  (define users (get-users))

  (gemini/success)
  (include-and-print "../../templates/menu.gmi")
  (show-auth)
  
  (print "## " #\x1f465 " Users" *CR* "\n" *CR*)

  (if (null? users)
    (print "No users registered" *CR*)
    (each-item user in users
      (print "=> users/" (gemini/escape-url (car user)) " " (car user) *CR*))))

(define (get-user-info _)
  (define user (gemini/unescape-url (car url-path)))

  (if (user-exists? user)
    (begin
      (gemini/success)
      (include-and-print "../../templates/menu.gmi")
      (show-auth)
      (display (get-user user)))
    (gemini/bad-request "User not found")))

(case (length url-path)
  ((0) => list-users)
  ((1) => get-user-info)
  (else (gemini/bad-request "Malformed user request")))

