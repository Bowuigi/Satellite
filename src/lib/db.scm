(module db *

(import scheme
        (chicken base)
        (chicken string)
        (chicken port)
        (chicken blob)
        (chicken condition)
        (chicken syntax)
        lmdb-ht)

;;; Constants
(define *current-db* (make-parameter #f))
(define *current-table* (make-parameter "main"))

;;; Syntaxes

;; Currently, lmdb-ht has a bug where it prints some debug information to
;; (current-output-port), which is fixed in the latest source. Until then,
;; we use parameterize to "hack around" it.

(define-syntax out-to-null
  (syntax-rules ()
    ((out-to-null
       body ...)
     (with-output-to-file "/dev/null"
        (lambda () body ...)))))

;; Convenient transaction handler
(define-syntax with-transaction
  (syntax-rules ()
    ((with-transaction
       body ...
       last)
     (begin (out-to-null (db-begin (*current-db*)))
            body ...
            (let ((last-value last))
              (db-end (*current-db*))
              last-value)))))


;;; Helpers
(define (tableize k . ks)
  (string-intersperse (cons (*current-table*) (cons k ks)) ":"))

(define (table! t . ts)
  (*current-table*
    (string-intersperse (cons t ts) ":")))

;;; General
(define (kset! k v)
  (db-set! (*current-db*) (string->blob (tableize k)) (string->blob v)))

(define (kget k)
  (blob->string (db-ref (*current-db*) (string->blob (tableize k)))))

(define (ktryset! k v)
  (condition-case (kget k)
    ((exn lmdb) (kset! k v))))

(define (kexists? k)
  (and
    (condition-case (kget k)
      ((exn lmdb) #f))
    #t))

(define (kget-list k)
  (call-with-input-string (kget k) read))

(define (kset-list! k v)
  (kset! k (call-with-output-string (cut write v <>))))

(define (kcons-list! x k)
  (kset-list! k (cons x (kget-list k))))

(define (kappend-list! k x)
  (kset-list! k (append (kget-list k) x)))

(define (debug) ;; Remove on release
  (with-transaction
    (db-for-each (lambda (k v) (print (blob->string k) " : " (blob->string v))) (*current-db*))))

;;; Init/end
(define (init! filename)
  (*current-db* (db-open filename))
  (table! "main")
  (with-transaction
    (ktryset! "users"
             (call-with-output-string (cut write '() <>)))
    (ktryset! "posts"
             (call-with-output-string (cut write '() <>)))
    (ktryset! "posts-by-rating"
             (call-with-output-string (cut write '() <>)))))

(define (end!)
  (db-close (*current-db*))
  (*current-db* #f))

) ; Module end

