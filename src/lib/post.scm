(module post *

(import scheme
        srfi-1
        common
        (chicken base)
        (chicken string)
        (chicken sort)
        (rename utf8 (print utf8-print))
        (prefix gemini gemini/)
        (prefix db db/)
        uuid)

(define-record post
               id
               author
               content
               categories
               timestamp)

(set-record-printer!
  post
  (lambda (this-post out)
    (parameterize ((current-output-port out))
      (print "### " (post-author this-post) *CR*)
      (print (post-content this-post) *CR* "\n" *CR*)
      (unless (null? (post-categories this-post))
        (print "Categories: "
          (string-intersperse
            (map (cut string-append "#" <>) (post-categories this-post))
            " ") *CR*))
      (print "Posted at " (utc-time-string (post-timestamp this-post)) *CR*)

      ;; Read most of those from DB directly
      (utf8-print "=> /posts/" (post-id this-post)
             "/upvote "
             #\x1f44d
             " Upvote (" (length (post-upvotes (post-id this-post))) ")" *CR*)
      (utf8-print "=> /posts/" (post-id this-post)
             "/downvote "
             #\x1f44e
             " Downvote (" (length (post-downvotes (post-id this-post))) ")"
             *CR*)

      (let ((comments (get-comments (post-id this-post))))
        (utf8-print "=> /posts/" (post-id this-post)
               "/comment "
               #\x1f4ac
               " Comment (" (length comments) ")"
               *CR*)

        (print *CR*)

        (each-item c in comments
            (print c *CR*))))))

(define (get-post id)
  (define prev-table (db/*current-table*))
  (define this-post #f)

  (db/table! "posts" id)

  (db/with-transaction
    (set! this-post (make-post
                      id
                      (db/kget "author")
                      (db/kget "content")
                      (db/kget-list "categories")
                      (string->number (db/kget "timestamp")))))

  (db/table! prev-table)
  this-post)

(define (get-posts-by-rating)
  (define prev-table (db/*current-table*))
  (define posts #f)

  (db/table! "main")
  (db/with-transaction
    (set! posts (map car (db/kget-list "posts-by-rating"))))

  (db/table! prev-table)
  posts)

(define (get-posts)
  (define prev-table (db/*current-table*))
  (define posts #f)

  (db/table! "main")
  (db/with-transaction
    (set! posts (db/kget-list "posts")))

  (db/table! prev-table)
  posts)

(define (new-post author content categories)
  (define prev-table (db/*current-table*))
  (define id (uuid)) 

  (db/table! "posts" id)
  (db/with-transaction
    (db/kset! "author" author)
    (db/kset! "content" content)
    (db/kset-list! "categories" categories)
    (db/kset! "timestamp" (number->string (timestamp)))
    (db/kset-list! "upvotes" '())
    (db/kset-list! "downvotes" '())
    (db/kset-list! "comments" '()))

  ;; Register post
  (db/table! "main")
  (db/with-transaction
    (db/kcons-list! id "posts")
    (db/kappend-list! "posts-by-rating" (list (cons id 0))))

  (db/table! prev-table))

(define (post-upvotes id)
  (define prev-table (db/*current-table*))
  (define upvotes #f)

  (db/table! "posts" id)

  (db/with-transaction
    (set! upvotes (db/kget-list "upvotes")))

  (db/table! prev-table)
  upvotes)

(define (upvote-post id username)
  (define prev-table (db/*current-table*))
  (define upvotes #f)
  (define downvotes #f)
  (define pbr #f)

  (db/table! "posts" id)

  (db/with-transaction
    (set! downvotes (db/kget-list "downvotes"))
    (set! upvotes (db/kget-list "upvotes"))

    (if (member username upvotes string=?)
      (set! upvotes (delete username upvotes string=?))
      (set! upvotes (cons username upvotes)))

    (db/kset-list! "upvotes" upvotes))

  (db/table! "main")

  (db/with-transaction
    (set! pbr (db/kget-list "posts-by-rating"))
    (set-cdr! (assoc id pbr string=?) (- (length upvotes) (length downvotes)))
    (db/kset-list!
      "posts-by-rating"
      (sort pbr (lambda (a b) (> (cdr a) (cdr b))))))

  (db/table! prev-table))

(define (post-downvotes id)
  (define prev-table (db/*current-table*))
  (define downvotes #f)

  (db/table! "posts" id)

  (db/with-transaction
    (set! downvotes (db/kget-list "downvotes")))

  (db/table! prev-table)
  downvotes)

(define (downvote-post id username)
  (define prev-table (db/*current-table*))
  (define upvotes #f)
  (define downvotes #f)
  (define pbr #f)

  (db/table! "posts" id)

  (db/with-transaction
    (set! downvotes (db/kget-list "downvotes"))
    (set! upvotes (db/kget-list "upvotes"))

    (if (member username downvotes string=?)
      (set! downvotes (delete username downvotes string=?))
      (set! downvotes (cons username downvotes)))

    (db/kset-list! "downvotes" downvotes))

  (db/table! "main")

  (db/with-transaction
    (set! pbr (db/kget-list "posts-by-rating"))
    (set-cdr! (assoc id pbr string=?) (- (length upvotes) (length downvotes)))
    (db/kset-list!
      "posts-by-rating"
      (sort pbr (lambda (a b) (> (cdr a) (cdr b))))))

  (db/table! prev-table))

;;; --- Comments ---

(define-record comment
               author
               content
               timestamp)

(set-record-printer!
  comment
  (lambda (this-comment out)
    (parameterize ((current-output-port out))
      (utf8-print "=> /users/" (gemini/escape-url (comment-author this-comment))
                  " " #\x1f5e8 " "
                  (comment-author this-comment)
                  " at " (utc-time-string (comment-timestamp this-comment))
                  " commented:" *CR*)
      (print (comment-content this-comment) *CR*))))

(define (get-comments id)
  (define prev-table (db/*current-table*))
  (define comments #f)

  (db/table! "posts" id)

  (db/with-transaction
    (set! comments (db/kget-list "comments")))
  
  (db/table! prev-table)

  (map (cut apply make-comment <>) comments))

(define (comment-post id author content)
  (define prev-table (db/*current-table*))

  (db/table! "posts" id)

  (db/with-transaction
    (db/kcons-list! (list author content (timestamp)) "comments"))

  (db/table! prev-table))

) ; Module end

