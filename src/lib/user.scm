(module user *

(import scheme
        common
        gemini
        (rename utf8 (print utf8-print))
        (chicken base)
        (chicken string)
        (prefix db db/))

(define-record user
               name
               description
               interests
               creation-ts
               tls-hash)

(set-record-printer!
  user
  (lambda (this-user out)
    (parameterize ((current-output-port out))
      (utf8-print "## " #\x1f464
                  " User " (user-name this-user) *CR* "\n" *CR*)
      (print "Description: " (user-description this-user) *CR*)
      (print "Created on: "
             (utc-time-string (user-creation-ts this-user)) *CR*)
      (unless (null? (user-interests this-user))
        (print "Interests:\n" *CR* "- "
               (string-intersperse (user-interests this-user) (string-append "\n" *CR* "- ")) *CR*)))))

(define (user-exists? name)
  (define prev-table (db/*current-table*))
  (define exists #f)

  (db/table! "users" name)
  (set! exists (db/with-transaction (db/kexists? "creation-ts")))

  (db/table! prev-table)
  exists)

(define (get-user name)
  (define prev-table (db/*current-table*))
  (define this-user #f)
  
  (db/table! "users" name)

  (db/with-transaction
    (set! this-user (make-user
                      name
                      (db/kget "description")
                      (db/kget-list "interests")
                      (string->number (db/kget "creation-ts"))
                      (db/kget "tls-hash"))))

  (db/table! prev-table)
  this-user)

(define (new-user name tls-hash #!optional (description "No description provided.") (interests '()))
  (define prev-table (db/*current-table*))

  ;; Store user
  (db/table! "users" name)

  ;; Check if user is already registered first
  (if (db/with-transaction (db/kexists? "creation-ts"))
    (begin
      (db/table! prev-table)
      #f)
    (begin
      ;; Actually store it
      (db/with-transaction
        (db/kset! "description" description)
        (db/kset-list! "interests" interests)
        (db/kset! "tls-hash" tls-hash)
        (db/kset! "creation-ts" (number->string (timestamp))))

      ;; Register user
      (db/table! "main")

      (db/with-transaction
        (db/kcons-list! (cons name tls-hash) "users")

      (db/table! prev-table)
      #t))))

(define (get-users)
  (define prev-table (db/*current-table*))
  (define users #f)

  (db/table! "main")
  (db/with-transaction
    (set! users (db/kget-list "users")))

  (db/table! prev-table)
  users)

(define (auth)
  (define users (get-users))

  (if (*tls-hash*)
    (let loop ((lst users))
      (cond
        ((null? lst)      #f)                 ; User not found
        ((string=?
           (cdar lst)                         ; Look for username in (name . hash)
           (*tls-hash*))  (caar lst))
        (else             (loop (cdr lst))))) ; Keep looking
    #f)) ; No TLS hash provided

(define (show-auth)
  (define name (auth))
  (if name
    (print "Logged in as " name *CR* "\n" *CR*)
    (print "Not logged in" *CR* "\n" *CR*)))

) ; Module end
