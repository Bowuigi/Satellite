(module common *

(import scheme
        utf8
        srfi-1
        (chicken base)
        (chicken condition)
        (chicken io)
        (chicken port)
        (chicken syntax)
        (chicken time)
        (chicken time posix))

;;; General

(define *CR* "\r")

(define-syntax each-item
  (syntax-rules (in)
    ((_ item in lst
           body ...)
     (for-each (lambda (item) body ...) lst))))

(define-syntax include-and-print
  (syntax-rules ()
    ((_ filename)
     (print (include filename) *CR*))))

;;; Time management

(define *human-ts-format* "%a %B %e %Y at %H:%M:%S UTC")

(define timestamp current-seconds)
(define (utc-time-string seconds)
  (time->string (seconds->utc-time seconds) *human-ts-format*))

(define (ensure-on-exit! proc)
  (on-exit proc)

  (current-exception-handler
    (let ((original-handler (current-exception-handler)))
      (lambda (exn)
        (proc)
        (original-handler exn)))))

;; Aliases
(define call/wis call-with-input-string)
(define call/wos call-with-output-string)

) ; Module end

