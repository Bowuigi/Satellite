(module gemini *

(import scheme
        (chicken base)
        (chicken process-context)
        (chicken irregex)
        common)

(define (unescape-url url)
  (irregex-replace/all '(seq "%" ($ (= 2 hex-digit)))
                       url
                       (lambda (m)
                         ((o string integer->char) (string->number (irregex-match-substring m 1) 16)))))

(define (escape-url url)
  (irregex-replace/all '(~ alphanumeric)
                       url
                       (lambda (m)
                         ((o (cut string-append "%" <>)
                             (cut number->string <> 16)
                             char->integer)
                          (string-ref (irregex-match-substring m) 0)))))

(define (parse-url-path path)
  (irregex-split "/" path))

(define *path* (make-parameter #f))
(define *query* (make-parameter #f))
(define *tls-hash* (make-parameter #f))

(define (empty? v)
  (or (eq? v #f)
      (string=? v "")))

(define (init!)
  (*path* (get-environment-variable "PATH_INFO"))
  (*query* (get-environment-variable "QUERY_STRING"))
  (*tls-hash* (get-environment-variable "TLS_CLIENT_HASH")))

(define (success)
  (print "20 text/gemini" *CR*))

(define (get-input prompt #!optional (password #f))
  (if password
    (print "11 " prompt *CR*)
    (print "10 " prompt *CR*)))

(define (redirect url)
  (print "32 " url *CR*))

(define (bad-request info)
  (print "59 " info *CR*))

(define (not-found info)
  (print "51 " info *CR*))

(define (cert-required info)
  (print "60 " info *CR*))

) ; Module end
