(import utf8
        common
        user
        post
        srfi-1
        (prefix db db/)
        (prefix gemini gemini/)
        (chicken string)
        (chicken irregex))

(db/init! *database-location*)
(ensure-on-exit! db/end!)

(gemini/init!)

(define username (auth))

(unless username
  (gemini/cert-required "Not logged in")
  (exit))

(when (gemini/empty? (gemini/*query*))
  (gemini/get-input "New post")
  (exit))

(define lines (irregex-split "\n" (gemini/unescape-url (gemini/*query*))))
(define categories '())

(when (char=? (string-ref (last lines) 0) #\#)
  (set! categories (irregex-split '("# ") (last lines)))
  (set! lines (butlast lines)))

(new-post username (string-intersperse lines "\n") categories)
(gemini/redirect "/latest")

