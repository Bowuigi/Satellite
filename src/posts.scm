(import utf8
        common
        user
        post
        (prefix db db/)
        (prefix gemini gemini/)
        (chicken string)
        (chicken irregex))

(db/init! *database-location*)
(ensure-on-exit! db/end!)

(gemini/init!)

(define username (auth))

(unless username
  (gemini/cert-required "Not logged in")
  (exit))

(define url-path
  (if (not (gemini/empty? (gemini/*path*)))
    (gemini/parse-url-path (gemini/*path*))
    '()))

(case (length url-path)
  ((0) (gemini/bad-request "Post id required")
       (exit))
  ((1) (gemini/bad-request "Operation required")
       (exit)))

(define (action-upvote _)
  (upvote-post (car url-path) username)
  (gemini/redirect "/popular"))

(define (action-downvote _)
  (downvote-post (car url-path) username)
  (gemini/redirect "/popular"))

(define (action-comment _)
  (if (gemini/empty? (gemini/*query*))
    (gemini/get-input "Comment")
    (begin (comment-post (car url-path)
                         username
                         (gemini/unescape-url (gemini/*query*)))
           (gemini/redirect "/popular"))))

(cond
  ((string=? (cadr url-path) "upvote")   => action-upvote)
  ((string=? (cadr url-path) "downvote") => action-downvote)
  ((string=? (cadr url-path) "comment")  => action-comment)
  (else (gemini/bad-request "Invalid operation")))

