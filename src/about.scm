(import common
        user
        (prefix db db/)
        (prefix gemini gemini/))

(db/init! *database-location*)
(ensure-on-exit! db/end!)

(gemini/init!)

(gemini/success)
(include-and-print "../../templates/menu.gmi")

(show-auth)

(include-and-print "../../templates/about.gmi")

