(import utf8
        srfi-1
        common
        user
        post
        (prefix db db/)
        (prefix gemini gemini/)
        (chicken string)
        (chicken irregex))

(db/init! *database-location*)
(ensure-on-exit! db/end!)

(gemini/init!)

(define url-path
  (if (not (gemini/empty? (gemini/*path*)))
    (gemini/parse-url-path (gemini/*path*))
    '("")))

;; Get filter input
(when (and (gemini/empty? (gemini/*query*))
           (or (string=? "author" (car url-path))
               (string=? "content" (car url-path))
               (string=? "category" (car url-path))))
  (gemini/get-input (string-append "Filter by " (car url-path)))
  (exit))

;; Display template
(gemini/success)
(include-and-print "../../templates/menu.gmi")
(show-auth)
(include-and-print "../../templates/popular.gmi")

(define posts (get-posts-by-rating))
(define query #f)
(when (gemini/*query*)
  (set! query (gemini/unescape-url (gemini/*query*))))

;; Filter posts
(cond
  ((equal? (car url-path) "author")
   (print "Filtering by author: " query *CR* "\n" *CR*)
   (set! posts (filter
                 (lambda (id)
                   (string=? (post-author (get-post id)) query)) posts)))
  ((equal? (car url-path) "content")
   (print "Filtering by content (glob): " query *CR* "\n" *CR*)
   (set! posts (filter
                 (lambda (id)
                   (irregex-search (sre->irregex (glob->sre query) 'i)
                                   (post-content (get-post id)))) posts)))
  ((equal? (car url-path) "category")
   (print "Filtering by category: " query *CR*)
   (set! posts (filter
                 (lambda (id)
                   (member query
                           (post-categories (get-post id)))) posts))))

;; Show posts (if any)
(when (null? posts)
  (print "No posts found" *CR*)
  (exit))

(each-item p in posts
  (display (get-post p)))

