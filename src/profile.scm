(import common
        user
        (rename utf8 (print utf8-print))
        (prefix db db/)
        (prefix gemini gemini/)
        (chicken string)
        (chicken irregex))

(db/init! *database-location*)
(ensure-on-exit! db/end!)

(gemini/init!)

(unless (gemini/*tls-hash*)
  (begin (gemini/cert-required "No certificate")
         (exit)))

(define url-path
  (if (not (gemini/empty? (gemini/*path*)))
    (gemini/parse-url-path (gemini/*path*))
    '()))

(define username (auth))
(define this-user #f)

(if username
  (set! this-user (get-user username))
  (begin (if (gemini/empty? (gemini/*query*))
           (gemini/get-input "Username")
           (if (new-user (gemini/unescape-url (gemini/*query*)) (gemini/*tls-hash*))
             (gemini/redirect "/profile")
             (gemini/bad-request "User already exists")))
         (exit)))

(define (change-info _)
  (cond
    ((string=? (car url-path) "description")
     (if (gemini/empty? (gemini/*query*))
       (gemini/get-input "Profile description")

       (db/with-transaction
         (db/table! "users" username)
         (db/kset! "description" (gemini/unescape-url (gemini/*query*)))
         (gemini/redirect "/profile"))))

    ((string=? (car url-path) "interests")
     (if (gemini/empty? (gemini/*query*))
       (gemini/get-input "Profile interests (space separated)")

       (db/with-transaction
         (db/table! "users" username)
         (db/kset-list!
           "interests"
           (irregex-split " " (gemini/unescape-url (gemini/*query*))))
         (gemini/redirect "/profile"))))

    (else (gemini/bad-request "Malformed user request"))))

(define (user-info _)
  (gemini/success)
  (include-and-print "../../templates/menu.gmi")
  (utf8-print "## " #\x1faaa " Profile" *CR*)
  (print "Username: " username *CR*)
  (print "Description: " (user-description this-user) *CR*)
  (print "=> /profile/description Change description" *CR*)
  (print "Created on: " (utc-time-string (user-creation-ts this-user)) *CR*)
  (let ((interests (user-interests this-user)))
    (unless (null? interests)
      (print "Interests:\n" *CR* "- "
             (string-intersperse interests (string-append "\n" *CR* "- ")) *CR*)))
  (print "=> /profile/interests Change interests" *CR*))

(case (length url-path)
  ((0) => user-info)
  ((1) => change-info)
  (else (gemini/bad-request "Malformed user request")))

