# Satellite

Gemini-based social network that is meant to be self-hosted.

Rewritten! Now in Scheme instead of Lua.

**Features**:

- Smooth and fast user experience
- Passwordless authentication (uses TLS certificates)
- Multiline posts and comments (length depends on the max query string length dictated by the server)
- Post filtering
- Profile editing, with descriptions and interests
- Very intuitive UI
- Upvotes and downvotes
- Full access to Gemtext markup inside posts and comments

# Screenshots

The screenshots were taken using [Lagrange](https://github.com/skyjake/lagrange).

- Satellite menu

![](/img/menu.png)

- Example post

![](/img/post.png)

- Creating a new post

![](/img/posting.gif)

- The post itself

![](/img/after-posting.png)

# Hosting

Satellite uses [Chicken Scheme](https://call-cc.org) (although it would not be hard to port to other schemes/lisps), so, for the compilation process, you will need it.

It also uses [LMDB](https://symas.com/lmdb) as a database, which is linked to as a shared library, so you will need it on the target system.

Satellite depends on the following eggs:

- srfi-1
- utf8
- lmdb-ht
- uuid

Which can be installed using:

```sh
chicken-install -s srfi-1 utf8 lmdb-ht uuid
```

Redefine the `SUDO` environment variable if you use `boas` or `doas`, check the `chicken-install(1)` manual for more information.

It is a CGI application, therefore, you need a CGI-supporting server.

The server must support the `TLS_CLIENT_HASH` CGI variable, otherwise authentication does not work.

The other required variables are `PATH_INFO` and `QUERY_STRING`.

After installing Chicken, LMDB, the eggs and the Gemini server, edit the **config.scm** file accordingly and run the following commands:

```sh
cd src/
make -j
```

The CGI binaries and the favicon will be in the serve directory. Now you no longer need Chicken Scheme nor the eggs, but LMDB is required to be in the target system.

Now move the binaries and the favicon to wherever you want to place them (as long as they are in the same folder) and host that folder with your gemini server of choice.

# Instance list

No instances yet!

# Help

Look at the help page in the Satellite instance (also available in templates/help.gmi), if that doesn't solve the problem, check the about page (also available in templates/about.gmi) for contact information.

